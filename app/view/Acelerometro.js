Ext.define('Ejemplo3Acelerometro.view.Acelerometro', {
    extend: 'Ext.form.Panel',
    alias: "widget.acelerometro",    
    requires: [
        'Ext.TitleBar',
        'Ext.form.*',
        'Ext.field.*'
    ],
    config: {
        width: '100%',
        height: '100%',        
        items: [{
            xtype: 'fieldset',
            title: 'Acelerometro',
            items: [{
                label : 'Y',
                itemId: 'yId',
                xtype: 'textfield',
                name: 'y'                
            },{
                label : 'X',
                itemId: 'xId',
                xtype: 'textfield',
                name: 'x'                
            },{
                label : 'Z',
                itemId: 'zId',
                xtype: 'textfield',
                name: 'z'                
            }]
        }]
    }
});
