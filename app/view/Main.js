Ext.define('Ejemplo3Acelerometro.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
        'Ext.Video'
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Acelerometro',
                iconCls: 'time',

                items: [{
                    xtype : 'toolbar',
                    docked: 'top',
                    items: [{
                        xtype: 'button',
                        ui: 'action',
                        text: 'Loop',
                        action: 'btnLoop',
                        iconMask: true,
                        iconCls: 'locate'
                    },
                    { xtype: "spacer" },
                    {
                        xtype: 'button',
                        ui: 'normal',
                        iconMask: true,
                        iconCls: 'more',
                        handler: function() {
     

                        }
                    }]
                    },
                    {
                        xtype: 'acelerometro'
                    }
                ]
            }

        ]
    }
});
