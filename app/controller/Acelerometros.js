Ext.define('Ejemplo3Acelerometro.controller.Acelerometros', {
    extend: 'Ext.app.Controller',
    //load stores
    config: {
        refs: {
            acelerometro: 'acelerometro',
            main: 'main'              
        },
        control: {
            'button[action=btnLoop]': {
                tap: 'loopAc'
            }
        }
    },
    launch: function() {
        var watchID = null;
    },
    stopListener: function(){

    },
    loopAc: function(){
        var view = this.getAcelerometro();

        var options = { frequency: 500 };

        watchID = navigator.accelerometer.watchAcceleration(function(acceleration) {
            //acceleration.timestamp
            view.down('#xId').setValue(acceleration.x);
            view.down('#yId').setValue(acceleration.y);
            view.down('#zId').setValue(acceleration.z);              

        }, onError, options);

    }
});