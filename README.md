#Sample App Mobile

##Sencha Touch 2 With Cordova 2.3

####Sencha MVC, Integration with Cordova API example.

>Communicate between View and Controller
  + Form With Z, X, Y Fields
  + From Controller Call Cordova API
>Cordova Api
  + API Local bd with SQLITE
  + Create, Read, Insert integration Sencha
>General App
  + List, Forms, Viewport Use
  + Call Phone Number from List View.
  + Call fire event with params from View

[Sencha](http://sencha.com/)
[Phonegap](http://phonegap.com/)

####Author
#####Mauricio Barria Joost
